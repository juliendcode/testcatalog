import { Component, OnInit, Input, Renderer2 } from '@angular/core';
import { VideoGame } from '../models/VideoGame';
import { GameService } from '../game.service';
import { SharedService } from '../shared.service';

// const ELEMENT_DATA: VideoGame[] = [
//   { id: '1', name: 'BURNIN RUBBER' },
//   { id: '2', name: 'IKARI WARRIOR' },
//   { id: '3', name: 'NORTH AND SOUTH' }
// ];

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit {
  @Input() products: any = [];
  private singleProduct;
  private isAdded;
  public catalogItems;

  // displayedColumns: string[] = ['id', 'name', 'delete'];

  constructor(
    private renderer: Renderer2,
    public gameService: GameService,
    public sharedService: SharedService
  ) {}

  // dataSource = this.gameService.getCatalog;

  // ngOnInit() {
  //   this.isAdded = new Array(this.products.length);
  //   this.isAdded.fill(false, 0, this.products.length);

  //   this.gameService.getProducts().subscribe(data => {
  //     if (data && data.length > 0) {
  //     } else {
  //       this.products.map((item, index) => {
  //         this.isAdded[index] = false;
  //       });
  //     }
  //   });
  // }

  // ngOnInit() {
  //   this.gameService.getProducts().subscribe(data => {
  //     this.catalogItems = data;
  //     console.log('cest dans catalog', data);
  //   });
  // }

  ngOnInit() {
    this.gameService.getProducts().subscribe(data => {
      this.catalogItems = data;

      // this.totalAmmount = this.sharedService.getTotalPrice();
    });
  }

  // ngOnInit() {
  //   this.isAdded = new Array(this.products.length);
  //   this.isAdded.fill(false, 0, this.products.length);

  //   this.gameService.getProducts().subscribe(data => {
  //     if (data && data.length > 0) {
  //     } else {
  //       this.products.map((item, index) => {
  //         this.isAdded[index] = false;
  //       });
  //     }
  //   });
  // }

  // Add item in cart on Button click
  //

  addToCart(event, productId) {
    if (event.target.classList.contains('mat-stroked-button')) {
      alert('This product is already added into cart.');
      return false;
    }

    // this.catalogItems.map((item, index) => {
    //   if (item.id === productId) {
    //     this.isAdded[index] = true;
    //   }
    // });

    this.singleProduct = this.catalogItems.filter(product => {
      return product.id === productId;
    });

    // this.cartItems.push(this.singleProduct[0]);

    this.sharedService.addProductToCart(this.singleProduct[0]);
  }
}
