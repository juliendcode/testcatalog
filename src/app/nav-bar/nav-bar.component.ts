import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  
  public cartProductCount: number = 0;

  constructor(private sharedService: SharedService) {}

  ngOnInit() {
    this.sharedService.getProducts().subscribe(data => {
      this.cartProductCount = data.length;
    });
  }
}
