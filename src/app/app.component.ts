import { Component, OnInit } from '@angular/core';
import { GameService } from './game.service';
import { SharedService } from './shared.service';
import { VideoGame } from './models/VideoGame';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'VideoGamesCatalog';

  products: any = [];

  // videoGames: any = [];

  constructor(
    private gameService: GameService,
    private sharedService: SharedService
  ) {}

  ngOnInit() {
    // Get all product list on component init
    this.gameService.getProducts().subscribe(data => {
      this.products = data.products;
      });
  }
}
