import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { FlexLayoutModule } from '@angular/flex-layout';

import {
  MatToolbarModule,
  MatSidenavModule,
  MatListModule,
  MatButtonModule,
  MatIconModule,
  MatTableModule,
  MatCardModule,
  MatSlideToggleModule,
  MatChipsModule,
  MatExpansionModule,
  MatInputModule,
  MatRippleModule
} from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';

//COMPONENTS
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { CatalogComponent } from './catalog/catalog.component';
import { FormComponent } from './form/form.component';
import { HttpClientModule } from '@angular/common/http';

// SERVICES
import { GameService } from './game.service';
import { SharedService } from './shared.service';
import { CartComponent } from './cart/cart.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    CatalogComponent,
    FormComponent,
    CartComponent
  ],
  imports: [
    BrowserModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,

    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    CdkTableModule,
    MatCardModule,
    MatListModule,
    MatButtonModule,
    MatInputModule,
    MatExpansionModule,
    MatToolbarModule,
    MatIconModule,
    MatSlideToggleModule,
    MatChipsModule,
    MatIconModule,
    MatRippleModule
  ],

  providers: [GameService, SharedService],
  bootstrap: [AppComponent]
})
export class AppModule {}
