import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { VideoGame } from './models/VideoGame';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  public cartItems = [];
  public products = new Subject();

  constructor() {
    let catalog = this.products.asObservable();
  }

  getProducts(): Observable<any> {
    return this.products.asObservable();
  }

  setProducts(products) {
    this.cartItems.push(...products);
    this.products.next(products);
  }

  addProductToCart(product) {
    this.cartItems.push(product);
    this.products.next(this.cartItems);
  }
  // addProductToCatalog(product) {
  //   this.catalogItems.push(product);
  //   this.products.next(this.cartItems);
  // }

  removeProductFromCart(productId) {
    this.cartItems.map((item, index) => {
      if (item.id === productId) {
        this.cartItems.splice(index, 1);
      }
    });

    // Update Observable value
    this.products.next(this.cartItems);
  }

  emptryCart() {
    this.cartItems.length = 0;
    this.products.next(this.cartItems);
  }

  getTotalPrice() {
    let total = 0;

    this.cartItems.map(item => {
      total += item.price;
    });

    return total;
  }
}
