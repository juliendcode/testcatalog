import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  public cartItems;
  public totalAmmount;

  constructor(private sharedService: SharedService) {}

  ngOnInit() {
    this.sharedService.getProducts().subscribe(data => {
      this.cartItems = data;

      this.totalAmmount = this.sharedService.getTotalPrice();
    });
  }

  // Remove item from cart list
  removeItemFromCart(productId) {
    /* this.cartItems.map((item, index) => {
      if (item.id === productId) {
        this.cartItems.splice(index, 1);
      }
    });

    this.mySharedService.setProducts(this.cartItems); */

    this.sharedService.removeProductFromCart(productId);
  }

  emptyCart() {
    this.sharedService.emptryCart();
  }
}
