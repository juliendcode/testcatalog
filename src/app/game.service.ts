import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { VideoGame } from './models/VideoGame';
import { Subject } from 'rxjs/Subject';

import { tap, map, filter } from 'rxjs/operators';
import 'rxjs/add/operator/map';

@Injectable()
export class GameService {
  // public dummyList: VideoGame[] = [
  //   {
  //     id: 1,
  //     name: 'Burnin Rubber',
  //     price: 22999,
  //     image: 'https://cpcrulez.fr/img/z1952.png',
  //     description:
  //       'This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.'
  //   }
  // ];
  // public products = new Subject();

  public catalogItems = [];
  public products = new Subject();

  // constructor(private http: HttpClient) {}

  // getProducts() {
  //   return this.http
  //     .get<any>('../assets/products.json')
  //     .map(response => response);
  // }

  getProducts(): Observable<any> {
    console.log('this.catalogItems  dans SHAREDSERVICE:', this.catalogItems);
    return this.products.asObservable();
  }

  addProductToCatalog(product) {
    this.catalogItems.push(product);
    this.products.next(this.catalogItems);
    console.log('CONSOLE DU SERVICE', product);
  } // getProducts(): Observable<any> {
  //   console.log('this.cartItems  dans SHAREDSERVICE:', this.products);
  //   return this.products.asObservable();
  // }
}

// public getCatalog(): VideoGame[] {
//   return this.catalog;
// }

// getProducts(): Observable<any> {
//   console.log('this.cartItems :', this.cartItems);
//   return this.products.asObservable();
// }
