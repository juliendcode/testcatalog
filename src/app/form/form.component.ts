import { Component, OnInit, Input } from '@angular/core';
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from '@angular/forms';
import { GameService } from '../game.service';
import { SharedService } from 'src/app/shared.service';
import { VideoGame } from '../models/VideoGame';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  // @Input() videoGame: VideoGame;

  @Input() videoGames: any = [];
  @Input() products: any = [];

  public gameForm: FormGroup;

  // gameForm: FormGroup;
  private isAdded;

  private singleProduct;
  submitted = false;

  constructor(
    public gameService: GameService,
    private sharedService: SharedService,
    private formBuilder: FormBuilder
  ) {}

  get f() {
    return this.gameForm.controls;
  }

  ngOnInit() {
    this.gameForm = this.formBuilder.group({
      name: ['', Validators.required],
      price: ['', Validators.required],
      description: ['', Validators.required]
    });
  }

  onSubmit() {
    let item = this.gameForm.value;
    console.log('CONSOLE DE FORM', item);
    // this.sharedService.addProductToCart(game);
    this.gameService.addProductToCatalog(item);
  }
}

// ngOnInit() {

// }

// onSubmit() {
//   this.singleProduct = this.videoGames.filter(videoGame => {
//     // console.log(this.videoGames);

//     return videoGame.id === videoGame;
//   });

//   // this.cartItems.push(this.singleProduct[0]);

//   this.sharedService.addProductToCatalog(this.singleProduct[0]);
// }

// <onSubmit() {
//   let product: VideoGame = this.gameForm.value;
//   this.sharedService.addProductToCatalog(product);
//   // console.log(product);
// }>>

// onSubmit() {
//   const game: VideoGame = this.gameForm.value;
//   // this.submitted = true;
//   // stop here if form is invalid
//   if (this.gameForm.invalid) {
//     return;
//   }
//   this.sharedService.addProductToCatalog(this.products).subscribe(
//     data => {
//       alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.gameForm.value));
//     },
//     error => console.log(error)
//   );
// }

// addToCatalog(event, productId) {
//   this.sharedService.addProductToCatalog(this.singleProduct[0]);
// }

// public addGame(): void {
//   this.gameService.addGame(this.name, this.price);
//   this.name = '';
//   this.price;
// }
